using Domain.Constants.User;
using Domain.Entities;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Persistence;

public class ThibaultAntoineDbContextInitializer
{
    const string DEFAULT_EMAIL = "admin@gmail.com";

    private readonly ILogger<ThibaultAntoineDbContextInitializer> _logger;
    private readonly ThibaultAntoineDbContext _context;
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<User> _userManager;

    public ThibaultAntoineDbContextInitializer(ILogger<ThibaultAntoineDbContextInitializer> logger,
        ThibaultAntoineDbContext context,
        RoleManager<Role> roleManager,
        UserManager<User> userManager)
    {
        _logger = logger;
        _context = context;
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    public async Task SeedAsync()
    {
        try
        {
            await SeedRoles();
            await SeedUsersAndMembersForRole(Roles.ADMINISTRATOR);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }

    private async Task SeedRoles()
    {
        if (!_roleManager.RoleExistsAsync(Roles.ADMINISTRATOR).Result)
            await _roleManager.CreateAsync(new Role { Name = Roles.ADMINISTRATOR, NormalizedName = Roles.ADMINISTRATOR.Normalize() });

        if (!_roleManager.RoleExistsAsync(Roles.MEMBER).Result)
            await _roleManager.CreateAsync(new Role { Name = Roles.MEMBER, NormalizedName = Roles.MEMBER.Normalize() });
    }

    private async Task SeedUsersAndMembersForRole(string role)
    {
        var user = await _userManager.FindByEmailAsync(DEFAULT_EMAIL);
        if (user == null)
        {
            user = BuildUser();
            var result = await _userManager.CreateAsync(user, "Qwerty123!");

            if (result.Succeeded)
                await _userManager.AddToRoleAsync(user, role);
            else
                throw new Exception($"Could not seed/create {role} user.");
        }

        var existingMember = _context.Members.IgnoreQueryFilters().FirstOrDefault(x => x.User.Id == user.Id);
        if (existingMember is { Active: true })
            return;

        if (existingMember == null)
        {
            var member = new Member("ADMIN", "MEMBER");
            member.SetUser(user);
            _context.Members.Add(member);
            await _context.SaveChangesAsync();
        }
        else if (!existingMember.Active)
        {
            existingMember.Activate();
            _context.Members.Update(existingMember);
            await _context.SaveChangesAsync();
        }
    }

    private User BuildUser()
    {
        return new User
        {
            Email = DEFAULT_EMAIL,
            UserName = DEFAULT_EMAIL,
            NormalizedEmail = DEFAULT_EMAIL.Normalize(),
            NormalizedUserName = DEFAULT_EMAIL,
            PhoneNumber = "555-555-5555",
            EmailConfirmed = true,
            PhoneNumberConfirmed = true,
            TwoFactorEnabled = false
        };
    }
}