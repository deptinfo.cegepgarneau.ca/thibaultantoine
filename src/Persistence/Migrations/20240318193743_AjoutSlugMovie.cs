﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    public partial class AjoutSlugMovie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieMember_Members_MemberId",
                table: "MovieMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieMember_Movies_MovieId",
                table: "MovieMember");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieMember",
                table: "MovieMember");

            migrationBuilder.RenameTable(
                name: "MovieMember",
                newName: "MovieMembers");

            migrationBuilder.RenameIndex(
                name: "IX_MovieMember_MovieId",
                table: "MovieMembers",
                newName: "IX_MovieMembers_MovieId");

            migrationBuilder.RenameIndex(
                name: "IX_MovieMember_MemberId",
                table: "MovieMembers",
                newName: "IX_MovieMembers_MemberId");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "Movies",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieMembers_Members_MemberId",
                table: "MovieMembers",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieMembers_Movies_MovieId",
                table: "MovieMembers",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieMembers_Members_MemberId",
                table: "MovieMembers");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieMembers_Movies_MovieId",
                table: "MovieMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers");

            migrationBuilder.DropColumn(
                name: "Slug",
                table: "Movies");

            migrationBuilder.RenameTable(
                name: "MovieMembers",
                newName: "MovieMember");

            migrationBuilder.RenameIndex(
                name: "IX_MovieMembers_MovieId",
                table: "MovieMember",
                newName: "IX_MovieMember_MovieId");

            migrationBuilder.RenameIndex(
                name: "IX_MovieMembers_MemberId",
                table: "MovieMember",
                newName: "IX_MovieMember_MemberId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieMember",
                table: "MovieMember",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieMember_Members_MemberId",
                table: "MovieMember",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieMember_Movies_MovieId",
                table: "MovieMember",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
