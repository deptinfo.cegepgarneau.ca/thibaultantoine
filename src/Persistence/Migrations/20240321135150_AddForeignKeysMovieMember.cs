﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    public partial class AddForeignKeysMovieMember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers");

            migrationBuilder.DropIndex(
                name: "IX_MovieMembers_MemberId",
                table: "MovieMembers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers",
                columns: new[] { "MemberId", "MovieId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieMembers",
                table: "MovieMembers",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovieMembers_MemberId",
                table: "MovieMembers",
                column: "MemberId");
        }
    }
}
