﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Application.Exceptions.MovieMember;

namespace Infrastructure.Repositories.MoviesMembers
{
    public class MovieMemberRepository : IMovieMemberRepository
    {
        private readonly IThibaultAntoineDbContext _context;

        public MovieMemberRepository(IThibaultAntoineDbContext context)
        {
            _context = context;
        }

        public List<MovieMember> GetMoviesByMember(Guid memberId, int skip, int take, bool descending, bool orderByName)
        {
            List<MovieMember> moviesMember = null;

            if (descending)
            {
                if (orderByName)
                {
                    moviesMember = _context.MovieMembers
                    .Include("Movie")
                    .Where(x => x.MemberId == memberId)
                    .OrderByDescending(x => x.Movie.Title)
                    .Skip(skip)
                    .Take(take)
                    .ToList();
                }
                else
                {
                    moviesMember = _context.MovieMembers
                    .Include("Movie")
                    .Where(x => x.MemberId == memberId)
                    .OrderByDescending(x => x.Created)
                    .Skip(skip)
                    .Take(take)
                    .ToList();
                }
            }
            else
            {
                if (orderByName)
                {
                    moviesMember = _context.MovieMembers
                    .Include("Movie")
                    .Where(x => x.MemberId == memberId)
                    .OrderBy(x => x.Movie.Title)
                    .Skip(skip)
                    .Take(take)
                    .ToList();
                }
                else
                {
                    moviesMember = _context.MovieMembers
                    .Include("Movie")
                    .Where(x => x.MemberId == memberId)
                    .OrderBy(x => x.Created)
                    .Skip(skip)
                    .Take(take)
                    .ToList();
                }
            }
            
            return moviesMember;
        }

        public List<MovieMember> GetMoviesByMemberAndTitle(Guid memberId, string title)
        {
            var moviesMember = _context.MovieMembers
                .Include("Movie")
                .Where(x => x.MemberId == memberId && x.Movie.Title.Contains(title))
                .OrderBy(x => x.Movie.Title)
                .ToList();
            return moviesMember;
        }

        public int GetMoviesByMemberCount(Guid memberId)
        {
            var moviesMemberCount = _context.MovieMembers.Where(x => x.MemberId == memberId).Count();
            return moviesMemberCount;
        }
    }
}
