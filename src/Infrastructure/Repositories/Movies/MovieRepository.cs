﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Slugify;
using Application.Exceptions.Movies;
using Application.Exceptions.MovieMember;

namespace Infrastructure.Repositories.Movies
{
    public class MovieRepository : IMovieRepository
    {
        private readonly IThibaultAntoineDbContext _context;
        private readonly ISlugHelper _slugHelper;

        public MovieRepository(IThibaultAntoineDbContext context, ISlugHelper slugHelper)
        {
            _context = context;
            _slugHelper = slugHelper;
        }

        public async Task CreateMovie(Movie movie)
        {
            GenerateSlug(movie);
            if (_context.Movies.Any(x => x.Title.Trim() == movie.Title.Trim()))
                throw new MovieWithTitleAlreadyExistsException($"A movie with title {movie.Title} already exists.");
            _context.Movies.Add(movie);

            await _context.SaveChangesAsync();
        }

        public async Task AddMemberLink(Movie movie, Domain.Entities.Member member)
        {
            if (movie.MembersLink is null)
                movie.MembersLink = new List<MovieMember>();

            MovieMember movieMember = new MovieMember
            {
                Movie = movie,
                Member = member
            };

            if (_context.MovieMembers.Any(x => x.MovieId == movieMember.Movie.Id && 
            x.MemberId == movieMember.Member.Id))
                throw new MovieMemberAlreadyExistsException(
                    $"A movie member with member id {movieMember.Member.Id} " +
                    $"and movie id {movieMember.Movie.Id} already exists.");

            movie.MembersLink.Add(movieMember);

            _context.Movies.Update(movie);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveMemberLink(Guid memberId, Guid movieId)
        {
            var movieMember = _context.MovieMembers.
                FirstOrDefault(x => x.MemberId == memberId && x.MovieId == movieId);

            if (movieMember == null)
                throw new MovieMemberNotFoundException(
                    $"Could not find a movie member with member id {memberId} and movie id {movieId}.");

            _context.MovieMembers.Remove(movieMember);
            await _context.SaveChangesAsync();
        }

        public Movie FindById(Guid id)
        {
            var movie = _context.Movies.AsNoTracking().FirstOrDefault(x => x.Id == id);
            if (movie == null)
                throw new MovieNotFoundException($"Could not find movie with id {id}.");
            return movie;
        }

        public Movie FindByTitle(string title)
        {
            var movie = _context.Movies.AsNoTracking().FirstOrDefault(x => x.Title == title);
            if (movie == null)
                throw new MovieNotFoundException($"Could not find movie with title {title}.");
            return movie;
        }

        public List<Movie> GetAll()
        {
            return _context.Movies.Include("Member").AsNoTracking().ToList();
        }

        private void GenerateSlug(Movie movie)
        {
            var slug = movie.Title;
            var slugs = _context.Movies.AsNoTracking().Where(x => x.Slug == slug).ToList();
            if (slugs.Any())
                slug = $"{slug}-{slug.Length + 1}";
            movie.SetSlug(_slugHelper.GenerateSlug(slug).Replace(".", ""));
        }
    }
}
