using Application.Exceptions.Members;
using Application.Interfaces;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Members;

public class MemberRepository : IMemberRepository
{
    private readonly IThibaultAntoineDbContext _context;

    public MemberRepository(IThibaultAntoineDbContext context)
    {
        _context = context;
    }

    public int GetMemberCount()
    {
        return _context.Members.Count();
    }

    public List<Member> GetAllWithUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .Where(x => x.User.Email == userEmail)
            .AsNoTracking()
            .ToList();
    }

    public List<Member> GetAll()
    {
        return _context.Members.Include(x => x.User).AsNoTracking().ToList();
    }

    public List<Member> GetAllNonAdmin()
    {
        var allMembers = _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .AsNoTracking()
            .ToList();
        var nonAdminMembers = allMembers.Where(x => !x.User.HasRole("admin")).ToList();
        return nonAdminMembers;
    }

    public Member FindById(Guid id)
    {
        var member = _context.Members
            .AsNoTracking()
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .Include(x => x.MoviesLink)
            .ThenInclude(x => x.Movie)
            .FirstOrDefault(x => x.Id == id);
        if (member == null)
            throw new MemberNotFoundException($"No member with id {id} was found.");
        return member;
    }

    public Member? FindByUserId(Guid userId, bool asNoTracking = true)
    {
        var query = _context.Members as IQueryable<Member>;
        if (asNoTracking)
            query = query.AsNoTracking();
        return query
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Id == userId);
    }

    public Member? FindByUserEmail(string userEmail)
    {
        return _context.Members
            .Include(x => x.User)
            .ThenInclude(x => x.UserRoles)
            .ThenInclude(x => x.Role)
            .FirstOrDefault(x => x.User.Email == userEmail);
    }

    public async Task CreateMember(Member member)
    {
        _context.Members.Add(member);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateMember(Member member)
    {   
        if (_context.Members.Include(x => x.User).Any(x => x.User.Email == member.User.Email && x.Id != member.Id))
           throw new Exception($"Un autre utilisateur � d�j� ce {member.User.Email}.");
        
        _context.Members.Update(member);
        await _context.SaveChangesAsync();
    }
    public async Task DeleteMember(Guid id)
    {
        var member = _context.Members.Include(x => x.User).FirstOrDefault(x => x.Id == id);

        if (member == null)
            throw new MemberNotFoundException($"Could not find member with id {id}.");

        _context.Members.Remove(member);
        await _context.SaveChangesAsync();
    }

    public bool DoesEmailAlreadyExists(string userEmail)
    {
        return _context.Members.Any(x => x.User.Email.ToUpper() == userEmail.ToUpper());
    }
}