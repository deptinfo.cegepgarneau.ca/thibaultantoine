﻿namespace Infrastructure.Repositories.Users.Exceptions;

public class PasswordException : Exception
{
    public PasswordException(string message) : base(message) { }
}