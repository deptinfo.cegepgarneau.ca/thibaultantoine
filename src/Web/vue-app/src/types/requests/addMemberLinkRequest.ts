﻿export interface IAddMemberLinkRequest {
  movieId?: string
  memberId: string
}
