﻿export interface IDeleteMemberRequest {
    firstName?: string
    lastName?: string
    email?: string
    password?: string
    avatar?: File
    id?: string
}