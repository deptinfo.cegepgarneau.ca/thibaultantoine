﻿export interface ICreateMemberRequest {
    firstName?: string
    lastName?: string
    email?: string
    password?: string
    avatar?: File
}
