﻿export interface IGetMoviesByMemberAndTitleRequest {
    memberId: string
    title: string
}

