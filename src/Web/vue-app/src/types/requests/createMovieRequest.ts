﻿export interface ICreateMovieRequest {
  title: string
  year?: string
  timeline?: string
  rating?: string
  image?: string
  memberId: string
}