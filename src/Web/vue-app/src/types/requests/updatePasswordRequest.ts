﻿export interface IUpdatePasswordRequest {
    password?: string
    oldPassword?: string
    id?: string
}
