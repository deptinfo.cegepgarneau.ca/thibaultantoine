﻿export interface IUpdateMemberRequest {
    firstName?: string
    lastName?: string
    email?: string
    avatar?: File
    id?: string
    password?: string
}
    