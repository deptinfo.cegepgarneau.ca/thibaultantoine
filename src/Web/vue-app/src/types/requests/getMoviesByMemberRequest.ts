﻿export interface IGetMoviesByMemberRequest {
    memberId: string
    skip: number
    take: number
    descending: boolean
    orderByName: boolean
}
