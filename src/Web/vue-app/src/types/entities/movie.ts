﻿export interface Movies {
  movies: Movie[]
}

export interface Movie {
  id?: string
  title: string
  year: string
  timeline: string
  rating?: string
  imdbRating?: string
  image?: string
  posterImage?: string
}