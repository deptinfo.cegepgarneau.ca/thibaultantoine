import {IPerson} from "@/types/entities/person";
import { MovieMember } from "./movieMember";

export class Member implements IPerson {
    id?: string
    crmId?: number
    firstName?: string
    lastName?: string
    email?: string
    userId?: string
    roles?: string[]
    avatar?: File
    savedAvatarImage?: string
    password?: string
    movieMembers?: MovieMember[]
}