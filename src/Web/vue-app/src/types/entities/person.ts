export interface IPerson {
  id?: string
  crmId?: number
  firstName?: string
  lastName?: string
  email?: string
}