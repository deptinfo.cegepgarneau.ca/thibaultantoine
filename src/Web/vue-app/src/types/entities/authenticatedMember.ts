export interface IAuthenticatedMember {
    firstName: string
    lastName: string
    email?: string
    roles: string[]
    memberNumber?: string
    id: string
    avatar:string
}