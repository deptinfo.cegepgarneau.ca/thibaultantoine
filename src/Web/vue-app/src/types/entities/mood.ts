﻿interface Coordonates {
    x: number
    y: number
}

export interface Mood {
    id: number
    title: string
    genres: string[],
    genreFR: string[],
    position: Coordonates
}

export const MOODS: Mood[] = [
    { id: 0, title: "Heureux", genres: ["comedy", "romance", "animation"], genreFR: ["Comédie", "Romance", "Animation"], position: {x: 0.5, y: 0}},
    { id: 1, title: "Concentré", genres: ["mystery", "sci-fi", "history"], genreFR: ["Enquête", "Science-fiction", "Film historique"], position: { x: 0.95, y: 0.05 }},
    { id: 2, title: "Détendu", genres: ["family", "music", "musical", "comedy"], genreFR: ["Famille", "Musique", "Comédie musicale", "Comédie"],  position: { x: 1, y: 0.5 }},
    { id: 3, title: "Fatigué", genres: ["documentary", "biography", "animation"], genreFR: ["Documentaire", "Biographie", "Animation"], position: { x: 0.95, y: 0.95 }},
    { id: 4, title: "Triste", genres: ["drama", "romance"], genreFR: ["Drame", "Romance"], position: { x: 0.5, y: 1 }},
    { id: 5, title: "Faché", genres: ["thriller", "war"], genreFR: ["Thriller", "Guerre"],  position: { x: 0.05, y: 0.95 }},
    { id: 6, title: "Térrifié", genres: ["horror", "film-noir"], genreFR: ["Horreur", "Film noir"], position: { x: 0, y: 0.5 }},
    { id: 7, title: "Excité", genres: ["action", "adventure", "fantasy", "sport"], genreFR: ["Action", "Aventure", "Fantastique", "Film de sport"], position: { x: 0.05, y: 0.05 }},
]