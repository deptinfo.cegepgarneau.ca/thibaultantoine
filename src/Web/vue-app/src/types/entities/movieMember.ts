﻿
export interface MovieMember {
    memberId: string
    movieId: string
    movie: {
        id: string,
        image: string,
        rating: string,
        timeline: string,
        title: string
        year: number
    }
}