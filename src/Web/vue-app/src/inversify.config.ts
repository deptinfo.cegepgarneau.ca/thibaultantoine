import { Container } from "inversify";
import axios, { AxiosInstance } from 'axios';
import "reflect-metadata";

import { TYPES } from "@/injection/types";
import {
    IApiService,
    IMemberService,
    IMovieService,
    IMovieMemberService
} from "@/injection/interfaces";
import {
    ApiService,
    MemberService,
    MovieService,
    MovieMemberService

} from "@/services";

const dependencyInjection = new Container();
dependencyInjection.bind<AxiosInstance>(TYPES.AxiosInstance).toConstantValue(axios.create())
dependencyInjection.bind<IApiService>(TYPES.IApiService).to(ApiService).inSingletonScope()
dependencyInjection.bind<IMemberService>(TYPES.IMemberService).to(MemberService).inSingletonScope()
dependencyInjection.bind<IMovieService>(TYPES.IMovieService).to(MovieService).inSingletonScope()
dependencyInjection.bind<IMovieMemberService>(TYPES.IMovieMemberService).to(MovieMemberService).inSingletonScope()

function useMemberService() {
    return dependencyInjection.get<IMemberService>(TYPES.IMemberService);
}

function useMovieService() {
    return dependencyInjection.get<IMovieService>(TYPES.IMovieService);
}

function useMovieMemberService() {
    return dependencyInjection.get<IMovieMemberService>(TYPES.IMovieMemberService);
}

export {
    dependencyInjection,
    useMemberService,
    useMovieService,
    useMovieMemberService
};