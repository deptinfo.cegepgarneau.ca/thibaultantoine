﻿import { Movies, Movie, Member } from "../types";

export function selectRandomMovie(movies: Movies) {
    const movieIndex = Math.floor(Math.random() * movies.movies.length);
    return movies.movies[movieIndex];
}

export function hasMemberSeenMovie(member: Member, movie: Movie) {
    if (!member.movieMembers)
        return false;

    const movieFound = member.movieMembers.find(memberMovie =>
        movie?.title === memberMovie.movie?.title
    );

    return movieFound !== undefined;
}
