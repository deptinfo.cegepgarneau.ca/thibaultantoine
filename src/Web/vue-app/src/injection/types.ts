export const TYPES = {
  IApiService: Symbol.for("IApiService"),
  IMemberService: Symbol.for("IMemberService"),
  IMovieService: Symbol.for("IMovieService"),
  IMovieMemberService: Symbol.for("IMovieMemberService"),
  AxiosInstance: Symbol.for("AxiosInstance")
};
