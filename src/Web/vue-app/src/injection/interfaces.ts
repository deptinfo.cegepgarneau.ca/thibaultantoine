// eslint-disable-next-line @typescript-eslint/no-empty-interface
import {
    ICreateMemberRequest,
    ICreateMovieRequest,
    IUpdateMemberRequest,
    IAddMemberLinkRequest,
    IGetMoviesByMemberRequest,
    IGetMoviesByMemberAndTitleRequest
} from "@/types/requests"
import { SucceededOrNotResponse } from "@/types/responses"
import { IAuthenticatedMember, Movies, Movie, Member, MovieMember } from "@/types/entities"
import { IUpdatePasswordRequest } from "@/types/requests/updatePasswordRequest"

export interface IApiService {
    headersWithJsonContentType(): any

    headersWithFormDataContentType(): any

    configRandomMovie(): any

    buildEmptyBody(): string
}

export interface IMemberService {
    getCurrentMember(): Promise<IAuthenticatedMember | undefined>

    getMemberWithId(memberId: string): Promise<Member>

    getMemberWithMoviesWithId(memberId: string): Promise<Member>

    getAllMembers(): Promise<Member[]>

    getAllNonAdmin(): Promise<Member[]>

    createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse>

    updateMember(request: IUpdateMemberRequest): Promise<SucceededOrNotResponse>

    updateMemberPassword(request: IUpdatePasswordRequest): Promise<SucceededOrNotResponse>

    deleteMember(memberId: string): Promise<SucceededOrNotResponse>

    doesEmailAlreadyExists(email: string): Promise<boolean>
}

export interface IMovieService {
    getRandomMovie(): Promise<Movies>

    getRandomMovieWithGenre(genre: string): Promise<Movies>

    createMovie(request: ICreateMovieRequest): Promise<SucceededOrNotResponse>

    addMemberLink(request: IAddMemberLinkRequest): Promise<SucceededOrNotResponse>

    removeMemberLink(movieId: string, memberId: string): Promise<SucceededOrNotResponse>

    getMovieByTitle(movieTitle: string): Promise<Movie>
}

export interface IMovieMemberService {
    getMoviesByMemberCount(memberId: string): Promise<number>

    getMoviesByMember(request: IGetMoviesByMemberRequest): Promise<MovieMember[]>

    getMoviesByMemberAndTitle(request: IGetMoviesByMemberAndTitleRequest): Promise<MovieMember[]>
}