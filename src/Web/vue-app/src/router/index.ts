import i18n from "@/i18n";
import { Role } from "@/types";
import { useMemberStore } from "@/stores/memberStore";
import { createRouter, createWebHistory } from "vue-router";

import Home from "../views/MoodMovies/Home.vue";
import RandomMovie from "../views/MoodMovies/RandomMovie.vue";
import MoodMovie from "../views/MoodMovies/MoodMovie.vue";
import MyMovies from "../views/MoodMovies/MyMovies.vue";

import Admin from "../views/admin/Admin.vue";
import AdminMemberIndex from "@/views/admin/AdminMemberIndex.vue";
import AdminAddMemberForm from "@/views/admin/AdminAddMemberForm.vue";
import AdminEditMemberForm from "@/views/admin/AdminEditMemberForm.vue";

import AddMemberForm from "@/views/Members/AddMemberForm.vue";
import UpdateMemberForm from "@/views/Members/UpdateMemberForm.vue"
import Profile from "@/views/Members/Profile.vue"

import Unauthorized from "@/components/Unauthorized.vue"
import NotFound from "@/components/NotFound.vue"

const router = createRouter({
    // eslint-disable-next-line
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return { top: 0 };
    },
    history: createWebHistory(),
    routes: [
        {
            path: i18n.t("routes.home.path"),
            name: "home",
            component: Home,
        },
        {
            path: i18n.t("routes.home.randomMovie.fullPath"),
            name: "randomMovie",
            component: RandomMovie,
        },
        {
            path: i18n.t("routes.home.moodMovie.fullPath"),
            name: "moodMovie",
            component: MoodMovie,
        },
        {
            path: i18n.t("routes.home.myMovies.fullPath"),
            name: "myMovies",
            component: MyMovies,
            meta: {
                requiresAuth: true,
                noLinkInBreadcrumbs: true,
            }
        },
        {
            path: i18n.t("routes.members.add.fullPath"),
            name: "members",
            component: AddMemberForm
        },
        {
            path: i18n.t("routes.members.update.fullPath"),
            name: "update",
            component: UpdateMemberForm
        },
        {
            path: i18n.t("routes.members.profile.fullPath"),
            name: "profile",
            component: Profile,
            meta: {
                requiresAuth: true,
                noLinkInBreadcrumbs: true,
            }
        },
        {
            path: i18n.t("routes.admin.path"),
            name: "admin",
            component: Admin,
            meta: {
                requiresAuth: true,
                requiredRole: Role.Admin,
                noLinkInBreadcrumbs: true,
            },
            children: [
                {
                    path: i18n.t("routes.admin.children.members.path"),
                    name: "admin.children.members",
                    component: Admin,
                    children: [
                        {
                            path: "",
                            name: "admin.children.members.index",
                            component: AdminMemberIndex
                        },
                        {
                            path: i18n.t("routes.admin.children.members.add.path"),
                            name: "admin.children.members.add",
                            component: AdminAddMemberForm
                        },
                        {
                            path: i18n.t("routes.admin.children.members.edit.path"),
                            name: "admin.children.members.edit",
                            component: AdminEditMemberForm
                        }
                    ]
                }
            ]
        },
        {
            path: '/unauthorized',
            name: 'Unauthorized',
            component: Unauthorized
        },
        {
            path: '/:pathMatch(.*)*',
            name: 'NotFound',
            component: NotFound
        }
    ]
});

// eslint-disable-next-line
router.beforeEach(async (to, from) => {
    const memberStore = useMemberStore()
    
    if (!memberStore.member.email && to.meta.requiresAuth)
        return {
            name: "home",
        };

    if (!to.meta.requiredRole)
        return;
    const isRoleArray = Array.isArray(to.meta.requiredRole)
    const doesNotHaveGivenRole = !isRoleArray && !memberStore.hasRole(to.meta.requiredRole as Role);
    const hasNoRoleAmountRoleList = isRoleArray && !memberStore.hasOneOfTheseRoles(to.meta.requiredRole as Role[]);
    if (doesNotHaveGivenRole || hasNoRoleAmountRoleList) {
        return {
            name: "Unauthorized",
        };
    }
});

export const Router = router;