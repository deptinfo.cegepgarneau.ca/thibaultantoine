import { AxiosResponse, AxiosError } from "axios"
import { injectable } from "inversify"

import "@/extensions/date.extensions"
import { ApiService } from "@/services/apiService"
import { IMemberService } from "@/injection/interfaces"
import { IAuthenticatedMember } from "@/types/entities/authenticatedMember"
import { ICreateMemberRequest } from "@/types/requests/createMemberRequest"
import { IUpdateMemberRequest } from "@/types/requests/updateMemberRequest"
import { IUpdatePasswordRequest } from "@/types/requests/updatePasswordRequest"
import { SucceededOrNotResponse } from "@/types/responses"
import { Member } from "../types"

@injectable()
export class MemberService extends ApiService implements IMemberService {
    public async getCurrentMember(): Promise<IAuthenticatedMember | undefined> {
        try {
            const response = await this
                ._httpClient
                .get<IAuthenticatedMember, AxiosResponse<IAuthenticatedMember>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/me`)
            return response.data
        } catch (error) {
            return Promise.reject(error)
        }
    }

    public async getMemberWithId(memberId: string): Promise<Member> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<Member> {
                return error.response as AxiosResponse<Member>;
            });
        return response.data as Member
    }

    public async getMemberWithMoviesWithId(memberId: string): Promise<Member> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member>>(`${process.env.VUE_APP_API_BASE_URL}/api/membersmovies/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<Member> {
                return error.response as AxiosResponse<Member>;
            });
        return response.data as Member
    }

    public async doesEmailAlreadyExists(email: string): Promise<boolean> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<boolean>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/email/${email}`)
            .catch(function (error: AxiosError): AxiosResponse<boolean> {
                return error.response as AxiosResponse<boolean>;
            });
        return response.data as boolean;
    }

    public async getAllMembers(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/members`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async getAllNonAdmin(): Promise<Member[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Member[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/members-no-admin`)
            .catch(function (error: AxiosError): AxiosResponse<Member[]> {
                return error.response as AxiosResponse<Member[]>
            })
        return response.data as Member[]
    }

    public async createMember(request: ICreateMemberRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateMemberRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/members`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async deleteMember(memberId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(`${process.env.VUE_APP_API_BASE_URL}/api/members/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async updateMember(request: IUpdateMemberRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .put<IUpdateMemberRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/members/${request.id}`,
                request,
                this.headersWithFormDataContentType())

            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async updateMemberPassword(request: IUpdatePasswordRequest): Promise<SucceededOrNotResponse> {
        console.log(request)
        const response = await this
            ._httpClient
            .patch<IUpdatePasswordRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/members/${request.id}`,
                request,
                this.headersWithFormDataContentType())

            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }
}