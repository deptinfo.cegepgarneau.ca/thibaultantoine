﻿import { IMovieService } from "@/injection/interfaces";
import { injectable } from "inversify";
import { ApiService } from "./apiService";
import { AxiosError, AxiosResponse } from "axios";
import { Movie, Movies } from "../types/entities/movie";
import { ICreateMovieRequest } from "../types/requests/createMovieRequest";
import { SucceededOrNotResponse } from "../types/responses";
import { IAddMemberLinkRequest } from "../types/requests/addMemberLinkRequest";

@injectable()
export class MovieService extends ApiService implements IMovieService {
    public async getRandomMovie(): Promise<Movies> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Movies>>(`https://moviesverse1.p.rapidapi.com/top-250-movies`,
                this.configRandomMovie())

            .catch(function (error: AxiosError): AxiosResponse<Movies> {
                return error.response as AxiosResponse<Movies>;
            });

        return response.data as Movies
    }

    public async getRandomMovieWithGenre(genre: string): Promise<Movies> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Movies>>(`https://moviesverse1.p.rapidapi.com/get-by-genre`,
                this.configRandomMovieWithGenre(genre))

            .catch(function (error: AxiosError): AxiosResponse<Movies> {
                return error.response as AxiosResponse<Movies>;
            });

        return response.data as Movies
    }

    public async createMovie(request: ICreateMovieRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateMovieRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/movies`,
                request, this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async addMemberLink(request: IAddMemberLinkRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .patch<IAddMemberLinkRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/movies/${request.movieId}/members/${request.memberId}`,
                request, this.headersWithJsonContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

    public async removeMemberLink(movieId: string, memberId: string): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .delete<AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/movies/${movieId}/members/${memberId}`)
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        return new SucceededOrNotResponse(response.status === 204)
    }

    public async getMovieByTitle(movieTitle: string): Promise<Movie> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<Movie>>(`${process.env.VUE_APP_API_BASE_URL}/api/movies/${movieTitle}`)
            .catch(
                function (error: AxiosError): AxiosResponse<Movie> {
                    return error.response as AxiosResponse<Movie>;
                });
        return response.data as Movie
    }

}