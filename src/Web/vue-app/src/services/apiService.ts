import {AxiosInstance} from "axios";
import {inject, injectable} from "inversify";

import {IApiService} from "@/injection/interfaces";
import {TYPES} from "@/injection/types";
import "@/extensions/date.extensions";
@injectable()
export class ApiService implements IApiService {
    _httpClient: AxiosInstance;

    constructor(@inject(TYPES.AxiosInstance) httpClient: AxiosInstance) {
    this._httpClient = httpClient;
  }

  public headersWithJsonContentType() {
    return {
      headers: {
            "Content-Type": 'application/json',
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
            "Access-Control-Allow-Headers": "Content-Type, Authorization"
      },
    };
  }

  public headersWithFormDataContentType() {
    return {
      headers: {
            "Content-Type": '"multipart/form-data"',
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
            "Access-Control-Allow-Headers": "Content-Type, Authorization"
      },
    };
  }

    public configRandomMovieWithGenre(genre: string) {
        return {
            headers: {
              'X-RapidAPI-Key': '68a5f11064msh174aa50cec40596p1aeaebjsne97037ad85ea',
              'X-RapidAPI-Host': 'moviesverse1.p.rapidapi.com'
            },
            params: {
                genre: genre
            }
        }
    }

    public configRandomMovie() {
        return {
          headers: {
            'X-RapidAPI-Key': '68a5f11064msh174aa50cec40596p1aeaebjsne97037ad85ea',
            'X-RapidAPI-Host': 'moviesverse1.p.rapidapi.com'
          }
        }
    }

  public buildEmptyBody(): string {
    return '{}'
  }
}