﻿import { IMovieMemberService } from "../injection/interfaces";
import { MovieMember } from "../types";
import { ApiService } from "./apiService";
import { injectable } from "inversify";
import { AxiosError, AxiosResponse } from "axios";
import { IGetMoviesByMemberRequest, IGetMoviesByMemberAndTitleRequest } from "../types/requests";

@injectable()
export class MovieMemberService extends ApiService implements IMovieMemberService {

    public async getMoviesByMemberCount(memberId: string): Promise<number> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<number>>(`/api/members/${memberId}/movies/count`)

            .catch(function (error: AxiosError): AxiosResponse<number> {
                return error.response as AxiosResponse<number>;
            });

        return response.data as number;
    }

    public async getMoviesByMember(request: IGetMoviesByMemberRequest): Promise<MovieMember[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<MovieMember[]>>(`/api/members/${request.memberId}/movies/${request.skip}/${request.take}/${request.descending}/${request.orderByName}`)

            .catch(function (error: AxiosError): AxiosResponse<MovieMember[]> {
                return error.response as AxiosResponse<MovieMember[]>;
            });

        return response.data as MovieMember[];
    }

    public async getMoviesByMemberAndTitle(request: IGetMoviesByMemberAndTitleRequest): Promise<MovieMember[]> {
        const response = await this
            ._httpClient
            .get<AxiosResponse<MovieMember[]>>(`/api/members/${request.memberId}/movies/${request.title}`)

            .catch(function (error: AxiosError): AxiosResponse<MovieMember[]> {
                return error.response as AxiosResponse<MovieMember[]>;
            });

        return response.data as MovieMember[];
    }

}
