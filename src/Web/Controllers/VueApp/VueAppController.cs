﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Cookies;

namespace Web.Controllers.VueAPp;

[Route("/app")]
public class VueAppController : BaseController
{
    public VueAppController(ILogger<VueAppController> logger) : base(logger)
    {
    }

    [HttpGet]
    public IActionResult Index()
    {
        var lang = HttpContext.GetCookieValue(CookieNames.LANGUAGE_COOKIE_NAME);
        return RedirectToAction("Index", lang == "fr" ? "FrenchVueApp" : "EnglishVueApp");
    }
}