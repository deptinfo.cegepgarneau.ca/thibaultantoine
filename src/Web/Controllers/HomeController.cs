﻿using Microsoft.AspNetCore.Mvc;
using Web.Extensions;

namespace Web.Controllers;

[Route("/")]
public class HomeController : BaseController
{
    public HomeController(ILogger<HomeController> logger) : base(logger)
    {
    }

    // GET
    public IActionResult Index()
    {
        return RedirectToAction("Index", "VueApp");
    }

    [HttpGet("/{**path}", Order = 999)]
    public IActionResult CatchAll()
    {
        return RedirectToAction("Index", "VueApp");
    }
}