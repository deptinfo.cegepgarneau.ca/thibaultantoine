﻿namespace Web.Features.Movies.RemoveMemberLink
{
    public class RemoveMemberLinkRequest
    {
        public Guid MovieId { get; set; }
        public Guid MemberId { get; set; }
        
    }
}
