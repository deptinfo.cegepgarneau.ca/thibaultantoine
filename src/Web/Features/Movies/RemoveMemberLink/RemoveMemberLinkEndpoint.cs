﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;

namespace Web.Features.Movies.RemoveMemberLink
{
    public class RemoveMemberLinkEndpoint : Endpoint<RemoveMemberLinkRequest, SucceededOrNotResponse>
    {
        private readonly IMovieRepository _movieRepository;

        public RemoveMemberLinkEndpoint(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Delete("/movies/{movieId}/members/{memberId}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(RemoveMemberLinkRequest request, CancellationToken cancellationToken)
        {
            await _movieRepository.RemoveMemberLink(request.MemberId, request.MovieId);
            await SendNoContentAsync(cancellationToken);
        }
    }
}
