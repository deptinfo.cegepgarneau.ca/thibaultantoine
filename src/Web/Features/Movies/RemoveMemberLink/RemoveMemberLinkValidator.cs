﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.Movies.RemoveMemberLink
{
    public class RemoveMemberLinkValidator : Validator<RemoveMemberLinkRequest>
    {
        public RemoveMemberLinkValidator() 
        {
            RuleFor(x => x.MovieId)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMovieId")
                .WithMessage("Movie id is required.");

            RuleFor(x => x.MemberId)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("Member id is required.");            
        }
    }
}
