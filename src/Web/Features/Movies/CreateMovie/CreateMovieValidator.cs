﻿using FastEndpoints;
using FluentValidation;
using Web.Extensions;

namespace Web.Features.Movies.CreateMovie
{
    public class CreateMovieValidator : Validator<CreateMovieRequest>
    {
        public CreateMovieValidator() 
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidTitle")
                .WithMessage("Le nom du titre ne peut pas être null ou vide.");
        }
    }
}
