﻿namespace Web.Features.Movies.CreateMovie
{
    public class CreateMovieRequest
    {
        public string Title { get; set; } = default!;
        public int Year { get; set; } = default!;
        public string Timeline { get; set; } = default!;
        public string Rating { get; set; } = default!;
        public string Image { get; set; } = default!;
        public string MemberId { get; set; } = default!;
    }
}
