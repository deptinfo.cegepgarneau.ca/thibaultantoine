﻿using Application.Interfaces.Services.Movies;
using Domain.Entities;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Movies.CreateMovie
{
    public class CreateMovieEndpoint : Endpoint<CreateMovieRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMovieCreationService _movieCreationService;

        public CreateMovieEndpoint(IMapper mapper, 
            IMovieCreationService movieCreationService)
        {
            _mapper = mapper;
            _movieCreationService = movieCreationService;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Post("movies");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CreateMovieRequest request, CancellationToken cancellationToken)
        {
            var movie = _mapper.Map<Movie>(request);
            await _movieCreationService.CreateMovie(movie);
            await SendOkAsync(new SucceededOrNotResponse(true), cancellationToken);
        }
    }
}
