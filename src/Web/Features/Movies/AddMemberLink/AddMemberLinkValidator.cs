﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.Movies.AddMemberLink
{
    public class AddMemberLinkValidator : Validator<AddMemberLinkRequest>
    {
        public AddMemberLinkValidator() 
        {
            RuleFor(x => x.MovieId)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMovieId")
                .WithMessage("Movie id is required.");

            RuleFor(x => x.MemberId)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("Member id is required.");
        }
    }
}
