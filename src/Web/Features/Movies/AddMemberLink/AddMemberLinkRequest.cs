﻿namespace Web.Features.Movies.AddMemberLink
{
    public class AddMemberLinkRequest
    {
        public Guid MovieId { get; set; }
        public Guid MemberId { get; set; }
    }
}
