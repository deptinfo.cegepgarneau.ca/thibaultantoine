﻿using Application.Interfaces.Services.Movies;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;

namespace Web.Features.Movies.AddMemberLink
{
    public class AddMemberLinkEndpoint : Endpoint<AddMemberLinkRequest, SucceededOrNotResponse>
    {
        private readonly IMovieMemberLinkService _movieMemberLinkService;

        public AddMemberLinkEndpoint(IMovieMemberLinkService movieMemberLinkService)
        {
            _movieMemberLinkService = movieMemberLinkService;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Patch("movies/{movieId}/members/{memberId}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(AddMemberLinkRequest request, CancellationToken cancellationToken)
        {
            await _movieMemberLinkService.AddMemberLink(request.MovieId, request.MemberId);
            await SendOkAsync(new SucceededOrNotResponse(true), cancellationToken);
        }
    }
}
