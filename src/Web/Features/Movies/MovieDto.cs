﻿namespace Web.Features.Movies
{
    public class MovieDto
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string Title { get; private set; } = default!;
        public int Year { get; private set; } = default!;
        public string Timeline { get; private set; } = default!;
        public string Rating { get; private set; } = default!;
        public string Image { get; private set; } = default!;
        public string Slug { get; private set; } = default!;
    }
}
