﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.Movies.GetMovie
{
    public class GetMovieValidator : Validator<GetMovieRequest>
    {
        public GetMovieValidator() 
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .NotNull()
                .WithErrorCode("EmptyMovieTitle")
                .WithMessage("Movie title is required");
        }
    }
}
