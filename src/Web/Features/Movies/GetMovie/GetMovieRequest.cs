﻿namespace Web.Features.Movies.GetMovie
{
    public class GetMovieRequest
    {
        public string Title { get; set; } = default!;
    }
}
