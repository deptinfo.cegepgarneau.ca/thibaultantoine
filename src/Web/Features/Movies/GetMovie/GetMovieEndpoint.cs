﻿using Application.Exceptions.Movies;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Movies.GetMovie
{
    public class GetMovieEndpoint : Endpoint<GetMovieRequest, MovieDto>
    {
        private readonly IMapper _mapper;
        private readonly IMovieRepository _movieRepository;

        public GetMovieEndpoint(IMapper mapper, IMovieRepository movieRepository)
        {
            _mapper = mapper;
            _movieRepository = movieRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("/movies/{title}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMovieRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var movie = _movieRepository.FindByTitle(request.Title);
                await SendOkAsync(_mapper.Map<MovieDto>(movie), cancellationToken);
            }
            catch (MovieNotFoundException)
            {
                await SendOkAsync(null, cancellationToken);
            }
            
        }
    }
}
