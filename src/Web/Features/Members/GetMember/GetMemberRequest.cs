﻿namespace Web.Features.Members.GetMember
{
    public class GetMemberRequest
    {
        public Guid Id { get; set; }
    }
}
