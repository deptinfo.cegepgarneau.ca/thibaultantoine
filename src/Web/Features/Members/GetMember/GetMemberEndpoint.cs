﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Members.GetMember
{
    public class GetMemberEndpoint : Endpoint<GetMemberRequest, MemberDTO>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetMemberEndpoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("members/{id}");
            //Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMemberRequest req, CancellationToken ct)
        {
            var members = _memberRepository.FindById(req.Id);
            await SendOkAsync(_mapper.Map<MemberDTO>(members), ct);
        }
    }
}
