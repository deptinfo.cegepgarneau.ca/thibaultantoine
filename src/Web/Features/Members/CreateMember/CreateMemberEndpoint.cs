﻿using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Repositories;
using FastEndpoints;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Members.CreateMember
{
    public class CreateMemberEndpoint : Endpoint<CreateMemberRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMemberCreationService _memberCreationService;
        private readonly IUserRepository _userRepository;

        public CreateMemberEndpoint(IMapper mapper, IMemberCreationService memberCreationService, IUserRepository userRepository)
        {
            _mapper = mapper;
            _memberCreationService = memberCreationService;
            _userRepository = userRepository;
        }

        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();

            Post("members");
            AllowAnonymous();
        }

        public override async Task HandleAsync(CreateMemberRequest request, CancellationToken cancellationToken)
        {
            User user = new User {
                Email = request.Email,
                UserName = request.Email,
                NormalizedEmail = request.Email.Normalize(),
                NormalizedUserName = request.Email,
                EmailConfirmed = true,
            };

            await _userRepository.CreateUser(user);
            await _userRepository.CreateUserPassword(user, request.Password);

            var member = _mapper.Map<Member>(request);
            member.SetUser(user);
            await _memberCreationService.CreateMember(member, request.Avatar);
            await SendOkAsync(new SucceededOrNotResponse(true), cancellationToken);
        }
    }
}
