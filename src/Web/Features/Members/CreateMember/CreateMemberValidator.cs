﻿using FastEndpoints;
using FluentValidation;

namespace Web.Features.Members.CreateMember
{
    public class CreateMemberValidator: Validator<CreateMemberRequest>
    {
        public CreateMemberValidator() 
        {
            RuleFor(x => x.FirstName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("Invalid firstname")
                .WithMessage("Firstname should not be empty or null.");

            RuleFor(x => x.LastName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("Invalid lastname")
                .WithMessage("Lastname should not be empty or null.");

            RuleFor(x => x.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress()
                .WithErrorCode("Invalid email")
                .WithMessage("Email should not be empty or null and must contains a '@'.");
        }
    }
}
