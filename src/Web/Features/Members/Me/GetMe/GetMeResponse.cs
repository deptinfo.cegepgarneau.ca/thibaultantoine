﻿namespace Web.Features.Members.Me.GetMe;

public class GetMeResponse
{
    public string FirstName { get; set; } = default!;
    public string LastName { get; set; } = default!;
    public string Email { get; set; } = default!;
    public List<string> Roles { get; set; } = default!;
    public Guid id { get; set; } = default!;
    public string avatar { get; set; } = default!;
}