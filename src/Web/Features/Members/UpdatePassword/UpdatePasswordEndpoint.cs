﻿using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Repositories;
using AutoMapper;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;
using Infrastructure.Repositories.Users;

namespace Web.Features.Members.UpdatePassword
{
    public class UpdatePasswordEndpoint : Endpoint<UpdatePasswordRequest, SucceededOrNotResponse>
    {
        
        private readonly IMapper _mapper;
        private readonly IMemberUpdateService _memberPasswordService;
        private readonly IUserRepository _userRepository;

        public UpdatePasswordEndpoint(IMapper mapper, IMemberUpdateService passwordUpdateService, IUserRepository userRepository)
        {
            _mapper = mapper;
            _memberPasswordService = passwordUpdateService;
            _userRepository = userRepository;
        }

        public override void Configure()
        {
            
            AllowFileUploads();
            DontCatchExceptions();

            Patch("members/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(UpdatePasswordRequest request, CancellationToken cancellationToken)
        {
            await _memberPasswordService.UpdatePassword(request.id, request.Password, request.oldPassword);
            await SendOkAsync(new SucceededOrNotResponse(true), cancellationToken);
        }
    }

 
}
