﻿namespace Web.Features.Members.UpdatePassword
{
    public class UpdatePasswordRequest
    {
        public string Password { get; set; } = default!;
        public string oldPassword { get; set; } = default!;
        public Guid id { get; set; } = default!;
    }
}
