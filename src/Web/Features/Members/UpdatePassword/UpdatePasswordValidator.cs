﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Members.UpdateMember;

namespace Web.Features.Members.UpdatePassword
{
    public class UpdatePasswordValidator : Validator<UpdatePasswordRequest>
    {
        public UpdatePasswordValidator() {
            RuleFor(x => x.id).NotEqual(Guid.Empty)
                .WithErrorCode("Empty member id")
                .WithMessage("Member id is required");
            RuleFor(x => x.Password).NotNull().NotEmpty()
                .WithErrorCode("Empty password")
                .WithMessage("Member password is required");
        }

    }
}
