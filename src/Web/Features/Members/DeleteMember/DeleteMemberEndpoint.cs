﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Members.DeleteMember
{
    public class DeleteMemberEndpoint : Endpoint<DeleteMemberRequest>
    {
        private readonly IMemberRepository _memberRepository;

        public DeleteMemberEndpoint(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }
        public override void Configure()
        {
            DontCatchExceptions();

            Delete("members/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(DeleteMemberRequest request, CancellationToken ct)
        {
            await _memberRepository.DeleteMember(request.Id);
            await SendNoContentAsync(ct);
        }

    }
}
