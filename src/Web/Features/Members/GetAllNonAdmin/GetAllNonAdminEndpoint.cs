﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Members.GetAllNonAdmin
{
    public class GetAllNonAdminEndpoint : EndpointWithoutRequest<List<MemberDTO>>
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;

        public GetAllNonAdminEndpoint(IMapper mapper, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("members-no-admin");
            Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            var members = _memberRepository.GetAllNonAdmin();
            await SendOkAsync(_mapper.Map<List<MemberDTO>>(members), ct);
        }
    }
}
