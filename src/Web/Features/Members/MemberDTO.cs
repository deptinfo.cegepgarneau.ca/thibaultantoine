﻿using Domain.Entities;
using Domain.Entities.Identity;
using Web.Features.Movies;
using Web.Features.MoviesMembers;

namespace Web.Features.Members
{
    public class MemberDTO
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public User User { get; set; } = default!;
        public string Avatar { get; set; } = default!;
        public List<MovieMemberDto> MovieMembers { get; set; } = default!;
    }
}
