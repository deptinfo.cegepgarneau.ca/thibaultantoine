﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.Members.DoesEmailAlreadyExists
{
    public class DoesEmailAlreadyExistsValidator : Validator<DoesEmailAlreadyExistsRequest>
    {
        public DoesEmailAlreadyExistsValidator() 
        {
            RuleFor(x => x.Email)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("InvalidEmail")
                .WithMessage("Email is required");
        }
    }
}
