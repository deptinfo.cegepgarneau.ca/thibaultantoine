﻿namespace Web.Features.Members.DoesEmailAlreadyExists
{
    public class DoesEmailAlreadyExistsRequest
    {
        public string Email { get; set; }
    }
}
