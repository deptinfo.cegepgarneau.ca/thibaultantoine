﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Web.Features.Members.DoesEmailAlreadyExists
{
    public class DoesEmailAlreadyExistsEndpoint : Endpoint<DoesEmailAlreadyExistsRequest, bool>
    {
        private readonly IMemberRepository _memberRepository;

        public DoesEmailAlreadyExistsEndpoint(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("members/email/{email}");
            AllowAnonymous();
        }

        public override async Task HandleAsync(DoesEmailAlreadyExistsRequest request, CancellationToken cancellationToken)
        {
            bool memberExists = _memberRepository.DoesEmailAlreadyExists(request.Email);
            await SendOkAsync(memberExists, cancellationToken);
        }
    }
}
