﻿using FastEndpoints;
using FluentValidation;
using Web.Features.Members.CreateMember;

namespace Web.Features.Members.UpdateMember
{
    public class UpdateMemberValidator: Validator<UpdateMemberRequest>
    {
        public UpdateMemberValidator()
        {
            RuleFor(x => x.FirstName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("Invalid firstname")
                .WithMessage("Firstname should not be empty or null.");

            RuleFor(x => x.LastName)
                .NotNull()
                .NotEmpty()
                .WithErrorCode("Invalid lastname")
                .WithMessage("Lastname should not be empty or null.");

            RuleFor(x => x.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress()
                .WithErrorCode("Invalid email")
                .WithMessage("Email should not be empty or null and must contains a '@'.");
        }
    }
}
