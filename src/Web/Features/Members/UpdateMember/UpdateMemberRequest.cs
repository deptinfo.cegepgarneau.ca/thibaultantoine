﻿namespace Web.Features.Members.UpdateMember
{
    public class UpdateMemberRequest
    {
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string PasswordHash { get; set; } = default!;
        public IFormFile Avatar { get; set; } = default!;
        public Guid id { get; set; } = default!;
    }
}
