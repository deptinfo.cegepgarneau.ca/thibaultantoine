﻿using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Entities.Identity;
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Members.UpdateMember
{
    public class UpdateMemberEndpoint : Endpoint<UpdateMemberRequest, SucceededOrNotResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMemberUpdateService _memberUpdateService;
        private readonly IUserRepository _userRepository;
        private readonly IMemberRepository _memberRepository;

        public UpdateMemberEndpoint(IMapper mapper, IMemberUpdateService memberUpdateService, IUserRepository userRepository, IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _memberUpdateService = memberUpdateService;
            _userRepository = userRepository;
            _memberRepository = memberRepository;
        }

        public override void Configure()
        {
            AllowFileUploads();
            DontCatchExceptions();

            Put("members/{id}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(UpdateMemberRequest request, CancellationToken cancellationToken)
        {
            var member = _mapper.Map<Member>(request);
            var currentMember = _memberRepository.FindById(member.Id);
            User? user = _userRepository.FindByEmail(currentMember.Email);
            if (user != null) { 
                user.Email = request.Email;
                user.NormalizedEmail = user.Email.ToUpper();
                user.UserName = request.Email;
                user.NormalizedUserName = user.UserName.ToUpper();
                member.SetUser(user);
                await _memberUpdateService.UpdateMember(member, request.Avatar);
                await _userRepository.UpdateUser(user);
            }
            await SendOkAsync(new SucceededOrNotResponse(true), cancellationToken);
        }
    }
}
