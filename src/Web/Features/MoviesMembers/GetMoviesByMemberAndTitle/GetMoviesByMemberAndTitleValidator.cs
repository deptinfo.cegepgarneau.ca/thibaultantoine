﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.MoviesMembers.GetMoviesByMemberAndTitle
{
    public class GetMoviesByMemberAndTitleValidator : Validator<GetMoviesByMemberAndTitleRequest>
    {
        public GetMoviesByMemberAndTitleValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("Member id is required");

            RuleFor(x => x.Title)
                .NotEmpty()
                .NotNull()
                .WithErrorCode("EmptyTitle")
                .WithMessage("Title is required");
        }
    }
}
