﻿namespace Web.Features.MoviesMembers.GetMoviesByMemberAndTitle
{
    public class GetMoviesByMemberAndTitleRequest
    {
        public Guid Id { get; set; }
        public string Title { get; set; } = default!;
    }
}
