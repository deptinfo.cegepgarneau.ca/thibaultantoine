﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.MoviesMembers.GetMoviesByMemberAndTitle
{
    public class GetMoviesByMemberAndTitleEndpoint : Endpoint<GetMoviesByMemberAndTitleRequest, List<MovieMemberDto>>
    {
        private readonly IMovieMemberRepository _movieMemberRepository;
        private readonly IMapper _mapper;

        public GetMoviesByMemberAndTitleEndpoint(IMovieMemberRepository movieMemberRepository, IMapper mapper)
        {
            _movieMemberRepository = movieMemberRepository;
            _mapper = mapper;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("/members/{id}/movies/{title}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMoviesByMemberAndTitleRequest request, CancellationToken cancellationToken)
        {
            var movies = _movieMemberRepository.GetMoviesByMemberAndTitle(request.Id, request.Title);
            await SendOkAsync(_mapper.Map<List<MovieMemberDto>>(movies), cancellationToken);
        }
    }
}
