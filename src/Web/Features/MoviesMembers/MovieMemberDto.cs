﻿using Web.Features.Movies;

namespace Web.Features.MoviesMembers
{
    public class MovieMemberDto
    {
        public Guid MemberId { get; set; }
        public Guid MovieId { get; set; }
        public MovieDto Movie { get; set; } = null!;
    }
}
