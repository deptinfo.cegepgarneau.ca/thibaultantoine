﻿namespace Web.Features.MoviesMembers.GetMoviesByMember
{
    public class GetMoviesByMemberRequest
    {
        public Guid Id { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public bool Descending { get; set; }
        public bool OrderByName { get; set; }
    }
}
