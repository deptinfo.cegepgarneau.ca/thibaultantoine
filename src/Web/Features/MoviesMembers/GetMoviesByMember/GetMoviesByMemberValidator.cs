﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.MoviesMembers.GetMoviesByMember
{
    public class GetMoviesByMemberValidator : Validator<GetMoviesByMemberRequest>
    {
        public GetMoviesByMemberValidator()
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("Member id is required");
        }
    }
}
