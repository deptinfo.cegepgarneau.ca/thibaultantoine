﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.MoviesMembers.GetMoviesByMember
{
    public class GetMoviesByMemberEndpoint : Endpoint<GetMoviesByMemberRequest, List<MovieMemberDto>>
    {
        private readonly IMovieMemberRepository _movieMemberRepository;
        private readonly IMapper _mapper;

        public GetMoviesByMemberEndpoint(IMovieMemberRepository movieMemberRepository, IMapper mapper)
        {
            _movieMemberRepository = movieMemberRepository;
            _mapper = mapper;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("/members/{id}/movies/{skip}/{take}/{descending}/{orderByName}");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMoviesByMemberRequest request, CancellationToken cancellationToken)
        {
            var movies = _movieMemberRepository.GetMoviesByMember(request.Id, request.Skip, request.Take, request.Descending, request.OrderByName);
            await SendOkAsync(_mapper.Map<List<MovieMemberDto>>(movies), cancellationToken);
        }
    }
}
