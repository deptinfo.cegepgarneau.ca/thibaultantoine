﻿using FastEndpoints;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Web.Features.MoviesMembers.GetMoviesByMemberCount
{
    public class GetMoviesByMemberCountValidator : Validator<GetMoviesByMemberCountRequest>
    {
        public GetMoviesByMemberCountValidator() 
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty)
                .WithErrorCode("EmptyMemberId")
                .WithMessage("Member id is required");
        }
    }
}
