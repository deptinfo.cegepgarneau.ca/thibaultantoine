﻿using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using Web.Features.Common;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.MoviesMembers.GetMoviesByMemberCount
{
    public class GetMoviesByMemberCountEndpoint : Endpoint<GetMoviesByMemberCountRequest, int>
    {
        private readonly IMovieMemberRepository _movieMemberRepository;
        private readonly IMapper _mapper;

        public GetMoviesByMemberCountEndpoint(IMovieMemberRepository movieMemberRepository, IMapper mapper)
        {
            _movieMemberRepository = movieMemberRepository;
            _mapper = mapper;
        }

        public override void Configure()
        {
            DontCatchExceptions();

            Get("/members/{id}/movies/count");
            AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public override async Task HandleAsync(GetMoviesByMemberCountRequest request, CancellationToken cancellationToken)
        {
            int moviesCount = _movieMemberRepository.GetMoviesByMemberCount(request.Id);
            await SendOkAsync(moviesCount, cancellationToken);
        }
    }
}
