﻿namespace Web.Features.MoviesMembers.GetMoviesByMemberCount
{
    public class GetMoviesByMemberCountRequest
    {
        public Guid Id { get; set; }
    }
}
