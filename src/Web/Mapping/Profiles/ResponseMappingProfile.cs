﻿using Application.Common;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Web.Features.Common;
using Web.Features.Members.Me.GetMe;
using Web.Features.Members;
using Web.Features.Movies;
using Web.Features.MoviesMembers;

namespace Web.Mapping.Profiles;

public class ResponseMappingProfile : Profile
{
    public ResponseMappingProfile()
    {
        CreateMap<IdentityResult, SucceededOrNotResponse>();

        CreateMap<IdentityError, Error>()
            .ForMember(error => error.ErrorType, opt => opt.MapFrom(identity => identity.Code))
            .ForMember(error => error.ErrorMessage, opt => opt.MapFrom(identity => identity.Description));

        CreateMap<Member, GetMeResponse>()
            .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.User.RoleNames))
            .ForMember(x => x.avatar, opt => opt.MapFrom(x => x.Avatar));

        CreateMap<Member, MemberDTO>()
            .ForMember(memberDTO => memberDTO.Created, opt => opt.MapFrom(member => member.User.Created.ToDateTimeUtc()))
            .ForMember(memberDTO => memberDTO.FirstName, opt => opt.MapFrom(member => member.FirstName))
            .ForMember(memberDTO => memberDTO.LastName, opt => opt.MapFrom(member => member.LastName))
            .ForMember(memberDTO => memberDTO.Email, opt => opt.MapFrom(member => member.Email))
            .ForMember(memberDTO => memberDTO.User, opt => opt.MapFrom(member => member.User))
            .ForMember(memberDTO => memberDTO.MovieMembers, opt => opt.MapFrom(member => member.MoviesLink));

        CreateMap<MovieMember, MovieMemberDto>()
            .ForMember(movieMemberDTO => movieMemberDTO.MovieId, opt => opt.MapFrom(movieMember => movieMember.MovieId))
            .ForMember(movieMemberDTO => movieMemberDTO.MemberId, opt => opt.MapFrom(movieMember => movieMember.MemberId))
            .ForMember(movieMemberDTO => movieMemberDTO.Movie, opt => opt.MapFrom(movieMember => movieMember.Movie));

        CreateMap<Movie, MovieDto>()
            .ForMember(movieDto => movieDto.Created, opt => opt.MapFrom(movie => movie.Created.ToDateTimeUtc()))
            .ForMember(movieDto => movieDto.Title, opt => opt.MapFrom(movie => movie.Title))
            .ForMember(movieDto => movieDto.Year, opt => opt.MapFrom(movie => movie.Year))
            .ForMember(movieDto => movieDto.Timeline, opt => opt.MapFrom(movie => movie.Timeline))
            .ForMember(movieDto => movieDto.Rating, opt => opt.MapFrom(movie => movie.Rating))
            .ForMember(movieDto => movieDto.Image, opt => opt.MapFrom(movie => movie.Image))
            .ForMember(movieDto => movieDto.Slug, opt => opt.MapFrom(movie => movie.Slug));
    }
}