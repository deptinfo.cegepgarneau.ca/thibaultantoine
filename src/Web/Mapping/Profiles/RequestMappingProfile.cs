﻿using AutoMapper;
using Domain.Common;
using Domain.Entities;
using Web.Dtos;
using Web.Features.Members.CreateMember;
using Web.Features.Members.UpdateMember;
using Web.Features.Movies.CreateMovie;

namespace Web.Mapping.Profiles;

public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {
        CreateMap<TranslatableStringDto, TranslatableString>().ReverseMap();

        CreateMap<CreateMemberRequest, Member>().ForMember(opt => opt.MoviesLink, opt => opt.Ignore());

        CreateMap<UpdateMemberRequest, Member>().ForMember(opt => opt.MoviesLink, opt => opt.Ignore());

        CreateMap<CreateMovieRequest, Movie>().ForMember(opt => opt.MembersLink, opt => opt.Ignore());
    }
}