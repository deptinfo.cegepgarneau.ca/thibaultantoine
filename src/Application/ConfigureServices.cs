﻿using System.Reflection;
using Application.Interfaces.Services.Members;
using Application.Interfaces.Services.Movies;
using Application.Interfaces.Services.Notifications;
using Application.Interfaces.Services.Users;
using Application.Services.Members;
using Application.Services.Movies;
using Application.Services.Notifications;
using Application.Services.Users;
using Application.Settings;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Slugify;

namespace Application;

public static class ConfigureServices
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());

        services.Configure<ApplicationSettings>(configuration.GetSection("Application"));

        services.AddScoped<ISlugHelper, SlugHelper>();

        services.AddScoped<INotificationService, EmailNotificationService>();

        services.AddScoped<IMemberCreationService, MemberCreationService>();
        services.AddScoped<IMemberUpdateService, MemberUpdateService>();

        services.AddScoped<IAuthenticatedUserService, AuthenticatedUserService>();
        services.AddScoped<IAuthenticatedMemberService, AuthenticatedMemberService>();

        services.AddScoped<IMovieCreationService, MovieCreationService>();
        services.AddScoped<IMovieMemberLinkService, MovieMemberLinkService>();

        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        return services;
    }
}