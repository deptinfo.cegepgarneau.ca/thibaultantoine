﻿namespace Application.Services.Notifications.Dtos;

public class SendNotificationResponseDto
{
    public bool Successful { get; }
    public IList<string> ErrorMessages { get; }

    public SendNotificationResponseDto(bool successful, IList<string>? errorMessages = null)
    {
        Successful = successful;
        ErrorMessages = errorMessages ?? new List<string>();
    }
}