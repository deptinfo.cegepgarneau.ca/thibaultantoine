﻿using Application.Interfaces.Services.Movies;
using Domain.Entities;
using Domain.Repositories;

namespace Application.Services.Movies
{
    public class MovieMemberLinkService : IMovieMemberLinkService
    {
        private readonly IMemberRepository _memberRepository;
        private readonly IMovieRepository _movieRepository;

        public MovieMemberLinkService(IMovieRepository movieRepository, IMemberRepository memberRepository)
        {
            _movieRepository = movieRepository;
            _memberRepository = memberRepository;
        }

        public async Task AddMemberLink(Guid movieId, Guid memberId)
        {
            Member member = _memberRepository.FindById(memberId);
            Movie movie = _movieRepository.FindById(movieId);

            await _movieRepository.AddMemberLink(movie, member);
        }
    }
}
