﻿using Application.Interfaces.Services.Movies;
using Domain.Entities;
using Domain.Repositories;

namespace Application.Services.Movies
{
    public class MovieCreationService : IMovieCreationService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieCreationService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task CreateMovie(Movie movie)
        {
            await _movieRepository.CreateMovie(movie);
        }
    }
}
