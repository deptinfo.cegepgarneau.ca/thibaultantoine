﻿using Application.Interfaces.FileStorage;
using Application.Interfaces.Services.Members;
using Application.Interfaces.Services.Users;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Members
{
    public class MemberUpdateService : IMemberUpdateService
    {
        private readonly IMemberRepository _memberRepository;
        private readonly IFileStorageApiConsumer _fileStorageApiConsumer;
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticatedUserService _authenticatedUserService;

        public MemberUpdateService(
            IMemberRepository memberRepository,
            IFileStorageApiConsumer fileStorageApiConsumer,
            IUserRepository userRepository,
            IAuthenticatedUserService authenticatedUserService)
                   
        {
            _memberRepository = memberRepository;
            _fileStorageApiConsumer = fileStorageApiConsumer;
            _userRepository = userRepository;
            _authenticatedUserService = authenticatedUserService;
        }

        public async Task UpdateMember(Member member, IFormFile? cardImage)
        {
            var existingMember = _memberRepository.FindById(member.Id);
            if (existingMember == null)
                throw new Exception($"Could not find book with id {member.Id}.");

            member = await UpdateAvatar(existingMember, member, cardImage);

            await _memberRepository.UpdateMember(member);
        }

        private async Task<Member> UpdateAvatar(Member existingMember, Member member, IFormFile? cardImage)
        {
            if (cardImage == null && existingMember.Avatar != null)
            {
                await _fileStorageApiConsumer.DeleteFileWithUrl(existingMember.Avatar);
                return member;
            }

            if (existingMember.Avatar != null)
            {
                await _fileStorageApiConsumer.DeleteFileWithUrl(existingMember.Avatar);
            }
            member.SetAvatarUrl(await _fileStorageApiConsumer.UploadFileAsync(cardImage));

            
            return member;
        }

        public async Task UpdatePassword(Guid id, string newPassword, string oldPassword)
        {
            await _authenticatedUserService.ChangeUserPassword(oldPassword, newPassword);
        }
    }
}
