﻿using Application.Interfaces.FileStorage;
using Application.Interfaces.Services.Members;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Members
{
    public class MemberCreationService : IMemberCreationService
    {
        private readonly IMemberRepository _memberRepository;
        private readonly IFileStorageApiConsumer _fileStorageApiConsumer;

        public MemberCreationService(
            IMemberRepository memberRepository, 
            IFileStorageApiConsumer fileStorageApiConsumer)
        {
            _memberRepository = memberRepository;
            _fileStorageApiConsumer = fileStorageApiConsumer;
        }

        public async Task CreateMember(Member member, IFormFile avatar)
        {
            if (member.Avatar != null)
            {
                member.SetAvatarUrl(await _fileStorageApiConsumer.UploadFileAsync(avatar));
            }

            await _memberRepository.CreateMember(member);
            member.Activate();
        }
    }
}
