﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions.MovieMember
{
    public class MovieMemberNotFoundException : Exception
    {
        public MovieMemberNotFoundException(string message) : base(message) { }
    }
}
