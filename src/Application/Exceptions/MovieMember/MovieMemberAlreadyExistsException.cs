﻿
namespace Application.Exceptions.MovieMember
{
    public class MovieMemberAlreadyExistsException : Exception
    {
        public MovieMemberAlreadyExistsException(string message) : base(message) { }
    }
}
