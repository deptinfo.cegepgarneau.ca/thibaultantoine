﻿
namespace Application.Exceptions.Movies
{
    public class MovieWithTitleAlreadyExistsException : Exception
    {
        public MovieWithTitleAlreadyExistsException(string message) : base(message) { }
    }
}
