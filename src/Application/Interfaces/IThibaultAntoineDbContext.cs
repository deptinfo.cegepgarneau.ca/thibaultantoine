﻿using Domain.Entities;
using Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces;

public interface IThibaultAntoineDbContext
{
    DbSet<Member> Members { get; }
    DbSet<Movie> Movies { get; }
    DbSet<MovieMember> MovieMembers { get; }

    Task<int> SaveChangesAsync(CancellationToken? cancellationToken = null);
}