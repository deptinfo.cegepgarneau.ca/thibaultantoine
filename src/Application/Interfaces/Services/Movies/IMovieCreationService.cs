﻿using Domain.Entities;

namespace Application.Interfaces.Services.Movies
{
    public interface IMovieCreationService
    {
        Task CreateMovie(Movie movie);
    }
}
