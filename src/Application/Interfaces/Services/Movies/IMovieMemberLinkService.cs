﻿
namespace Application.Interfaces.Services.Movies
{
    public interface IMovieMemberLinkService
    {
        Task AddMemberLink(Guid movieId, Guid memberId);
    }
}
