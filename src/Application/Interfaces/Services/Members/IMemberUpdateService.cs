﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces.Services.Members
{
    public interface IMemberUpdateService
    {
        Task UpdateMember(Member member, IFormFile? avatar);
        Task UpdatePassword(Guid id, string newPassword, string oldPassword);
    }
}
