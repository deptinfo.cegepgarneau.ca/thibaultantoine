﻿using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces.Services.Members
{
    public interface IMemberCreationService
    {
        Task CreateMember(Member member, IFormFile avatar);
    }
}
