﻿using System.Text.RegularExpressions;

namespace Application.Helpers;

public static class PhoneNumberHelper
{
    public static bool IsValidPhoneNumber(this string? number)
    {
        const string PHONE_NUMBER_PATTERN = @"^[0-9]{3}-[0-9]{3}-[0-9]{4}$";
        return Regex.IsMatch(number ?? "", PHONE_NUMBER_PATTERN);
    }
}