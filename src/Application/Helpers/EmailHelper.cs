﻿using System.Text.RegularExpressions;

namespace Application.Helpers;

public static class EmailHelper
{
    public static bool IsValidEmail(this string? email)
    {
        const string EMAIL_PATTERN = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";
        return Regex.IsMatch(email?.Trim() ?? "", EMAIL_PATTERN);
    }
}