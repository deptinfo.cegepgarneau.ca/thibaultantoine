﻿using Domain.Common;
using Domain.Entities.Identity;
using Domain.Extensions;

namespace Domain.Entities;

public class Member : AuditableAndSoftDeletableEntity, ISanitizable
{
    public string FirstName { get; private set; } = default!;
    public string LastName { get; private set; } = default!;
    public string FullName => $"{FirstName} {LastName}";
    public string Email => User.Email;
    public User User { get; private set; } = default!;
    public string? Avatar { get; private set; } = default!;
    public bool Active { get; private set; }
    public List<MovieMember> MoviesLink { get; set; } = default!;

    public Member() {}

    public Member(string firstName, string lastName)
    {
        FirstName = firstName;
        LastName = lastName;
    }

    public void SetFirstName(string firstName) => FirstName = firstName;
    public void SetLastName(string lastName) => LastName = lastName;
    public void SetUser(User user) => User = user;
    public void SetAvatarUrl(string avatarUrl) => Avatar = avatarUrl;

    public void OnCreated(User user)
    {
        SetUser(user);
    }

    public void Activate()
    {
        Restore();
        Active = true;
        User.Activate(FirstName);
    }

    public void Deactivate(string? deletedBy = null)
    {
        Active = false;
        User.SoftDelete(deletedBy);
    }

    public override void SoftDelete(string? deletedBy = null)
    {
        base.SoftDelete(deletedBy);
        Active = false;
        User.SoftDelete(deletedBy);
    }

    public void SanitizeForSaving()
    {
        FirstName = FirstName.Trim().CapitalizeFirstLetterOfEachWord()!;
        LastName = LastName.Trim().CapitalizeFirstLetterOfEachWord()!;
    }
}