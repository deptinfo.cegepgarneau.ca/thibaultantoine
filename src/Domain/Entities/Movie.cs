﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities
{
    public class Movie : AuditableAndSoftDeletableEntity
    {
        public string Title { get; private set; } = default!;
        public int Year { get; private set; } = default!;
        public string Timeline {  get; private set; } = default!;
        public string Rating { get; private set; } = default!;
        public string Image { get; private set; } = default!;
        public List<MovieMember> MembersLink { get; set; } = default!;
        public string Slug { get; private set; } = default!;

        public void SetTitle(string title) => Title = title;
        public void SetYear(int year) => Year = year;
        public void SetTimeline(string timeline) => Timeline = timeline;
        public void SetRating(string rating) => Rating = rating;
        public void SetImage(string image) => Image = image;
        public void SetSlug(string value) => Slug = value;

        public void SanitizeForSaving()
        {
            Title = Title.Trim();
            Timeline = Timeline.Trim();
            Rating = Rating.Trim();
        }
    }
}
