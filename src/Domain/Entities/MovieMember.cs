﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities
{
    public class MovieMember : AuditableEntity
    {
        public Guid MemberId { get; set; }
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; } = null!;
        public Member Member { get; set; } = null!;
    }
}
