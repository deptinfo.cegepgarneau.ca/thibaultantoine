﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IMovieRepository
    {
        List<Movie> GetAll();
        Movie FindById(Guid id);
        Movie FindByTitle(string title);
        Task CreateMovie(Movie movie);
        Task AddMemberLink(Movie movie, Member member);
        Task RemoveMemberLink(Guid memberId, Guid movieId);
    }
}
