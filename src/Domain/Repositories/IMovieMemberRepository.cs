﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IMovieMemberRepository
    {
        int GetMoviesByMemberCount(Guid memberId);
        List<MovieMember> GetMoviesByMember(Guid memberId, int skip, int take, bool descending, bool orderByName);
        List<MovieMember> GetMoviesByMemberAndTitle(Guid memberId, string title);
    }
}
