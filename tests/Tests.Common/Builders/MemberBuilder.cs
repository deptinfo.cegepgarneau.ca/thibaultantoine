﻿using Domain.Entities;
using Domain.Entities.Identity;
using Domain.ValueObjects;
using NodaTime;

namespace Tests.Common.Builders;

public class MemberBuilder
{
    private const string ANY_FIRST_NAME = "john";
    private const string ANY_LAST_NAME = "doe";
    private const string ANY_EMAIL = "john.doe@gmail.com";

    private Guid? Id { get; set; }
    private string? FirstName { get; set; }
    private string? LastName { get; set; }
    private string? Email { get; set; }
    private Instant? Deleted { get; set; }
    private string? DeletedBy { get; set; }
    private User? User { get; set; }
    private bool? Active { get; set; }
    public string? Avatar { get; set; }

    public MemberBuilder WithId(Guid id)
    {
        Id = id;
        return this;
    }

    public MemberBuilder WithFirstName(string firstName)
    {
        FirstName = firstName;
        return this;
    }

    public MemberBuilder WithLastName(string lastName)
    {
        LastName = lastName;
        return this;
    }

    public MemberBuilder WithEmail(string email)
    {
        Email = email;
        return this;
    }

    public MemberBuilder WithUser(User user)
    {
        User = user;
        return this;
    }

    public MemberBuilder WithActive(bool active)
    {
        Active = active;
        return this;
    }

    public Member Build()
    {
        var member = new Member(
            FirstName ?? ANY_FIRST_NAME,
            LastName ?? ANY_LAST_NAME
        );

        User ??= new User();
        User.Email = Email ?? ANY_EMAIL;
        member.SetUser(User);

        if (Active.HasValue && Active.Value)
            member.Activate();

        if (Active.HasValue && Active.Value)
            member.Activate();

        if (DeletedBy != null || Deleted != null)
            member.SoftDelete(DeletedBy);

        member.SetId(Id ?? Guid.Empty);

        member.SetAvatarUrl("");

        return member;
    }
}